# SimpleStockApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.3.

Simple stock app allows to visualize the prices of the given symbols [GOOG , AAPL, MSFT, TSLA] in real time as well as a history of these prices for previous periods.

- Real Time prices are received from the websocket of yahoo-finance and decoded with protobufJs package following rules in the YPricingData file in assets folder.

- Historical prices [daily & 52week] are taken from a backend nodejs server, it's an api that we've developed to listen on yahoo-finance api and extrat prices data.

- To get daily prices and 52 week prices you have to run the backend server on the same machine. To clone the backend server (https://gitlab.com/ccgroup3/backend-nodejs-yfinance-api.git)

## Structure

We have three modules :

- SharedModule for shared components : spinner + header
- CoreModule for sigleton : services (http-prices, websocket-prices, data-prices) , models
- StockPrices module for prices-list component and it's children price-card + toggle button

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
