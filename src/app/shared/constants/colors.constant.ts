export const toggleDisabled = '#918585';
export const toggleAvailable = '#56d38a';
export const cardDisabled = '#e2e0e0';
export const cardAvailable = '#95d8b1';
