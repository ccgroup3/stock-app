import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PricesListComponent } from './prices-list/prices-list.component';

const routes: Routes = [
  { path: '/prices', component: PricesListComponent },
  {
    path: '',
    redirectTo: '/prices',
  },
  {
    path: '**',
    component: PricesListComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StockPricesRoutingModule {}
