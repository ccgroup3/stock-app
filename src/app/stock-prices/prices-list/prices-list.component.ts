import { OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Price } from '@core/models/price.model';
import { HttpPricesService } from '@core/services/http-prices.service';
import { PricesDataService } from '@core/services/prices-data.service';

@Component({
  selector: 'app-prices-list',
  templateUrl: './prices-list.component.html',
  styleUrls: ['./prices-list.component.scss'],
})
export class PricesListComponent implements OnInit, OnDestroy {
  prices: Partial<Price>[] = [];
  spinner: boolean = true;
  subscription: Subscription;
  constructor(
    private pricesService: PricesDataService,
    private httpServicePrices: HttpPricesService
  ) {}
  ngOnInit(): void {
    this.emitPrices();
    this.emitHighLowPrices();
  }
  /**
   * Emit high & low Prices
   */
  private emitHighLowPrices(): void {
    const symbols = ['AAPL', 'TSLA', 'MSFT', 'GOOG'];
    this.httpServicePrices
      .getYahooFinancePrices(JSON.stringify(symbols))
      .subscribe(
        (res) => {
          for (let index = 0; index < res.length; index++) {
            const symbol = res[index];
            const symbolIndex = this.prices.findIndex(
              (item) => item['id'] === symbol['id']
            );
            symbolIndex !== -1
              ? (this.prices[symbolIndex] = {
                  ...this.prices[symbolIndex],
                  weekly: symbol['daily'],
                  daily: symbol['daily'],
                })
              : this.prices.push(symbol);
          }
          this.spinner = false;
        },
        (err) => {
          console.log({ err });
        }
      );
  }
  /**
   * Push Symbol WS Price into Prices list
   */
  private emitPrices(): void {
    this.subscription = this.pricesService.prices.subscribe(
      async (msg) => {
        const decodedData = await this.pricesService.decodeData(msg);
        this.spinner = false;
        if (
          decodedData &&
          this.prices?.findIndex((price) => price.id === decodedData['id']) ===
            -1
        ) {
          this.prices.push({
            id: decodedData['id'],
            price: decodedData['price'].toFixed(4),
          });
        } else {
          this.prices?.map((price) => {
            if (price?.id === decodedData['id']) {
              price.price = decodedData['price'].toFixed(4);
            }
          });
        }
      },
      (err) => {
        console.log({ err });
      }
    );
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
