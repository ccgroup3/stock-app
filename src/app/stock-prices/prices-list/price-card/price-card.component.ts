import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { cardAvailable, cardDisabled } from '@shared/constants/colors.constant';

@Component({
  selector: 'app-price-card',
  templateUrl: './price-card.component.html',
  styleUrls: ['./price-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PriceCardComponent implements OnInit, OnChanges {
  offStock: boolean;
  cardColor: string;
  //= !this.offStock ? cardAvailable : cardDisabled; //initially the stock is available (green card)
  @Input() id: string = '';
  @Input() price: number | string;
  @Input() highestDaily: number | string;
  @Input() lowestDaily: number | string;
  @Input() highestWeekly: number | string;
  @Input() lowestWeekly: number | string;
  constructor() {}
  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges) {
    this.offStock = JSON.parse(localStorage.getItem(this.id));
    this.cardColor =
      this.offStock && this.offStock['disable']
        ? cardDisabled
        : this.offStock && !this.offStock['disable']
        ? cardAvailable
        : cardAvailable;
  }

  outOfStockdetection(disable) {
    if (!disable) {
      console.log('true', disable);
      this.cardColor = cardAvailable;
    } else {
      this.cardColor = cardDisabled;
    }
  }
}
