import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  SimpleChanges,
  OnChanges,
} from '@angular/core';
import {
  toggleAvailable,
  toggleDisabled,
} from '@shared/constants/colors.constant';

@Component({
  selector: 'app-toggle-button',
  templateUrl: './toggle-button.component.html',
  styleUrls: ['./toggle-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToggleButtonComponent implements OnInit, OnChanges {
  @Input() id: string;
  @Output() outOfStock = new EventEmitter();
  offStock: boolean;
  color: string; //initially the stock is available (green Button)
  disable: boolean;
  constructor() {}
  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges) {
    this.offStock = JSON.parse(localStorage.getItem(this.id));
    this.disable =
      this.offStock && this.offStock['disable']
        ? this.offStock['disable']
        : false;

    this.color =
      this.offStock && this.offStock['disable']
        ? toggleDisabled
        : this.offStock && !this.offStock['disable']
        ? toggleAvailable
        : toggleAvailable;
  }
  /**
   * Manage Stock avalability
   */
  changeStockAvailability() {
    this.disable = !this.disable;
    this.color = this.disable ? toggleDisabled : toggleAvailable;
    localStorage.setItem(
      this.id,
      JSON.stringify({ id: this.id, disable: this.disable })
    );
    this.outOfStock.emit(this.disable);
  }
}
