import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StockPricesRoutingModule } from './stock-prices-routing.module';
import { PricesListComponent } from './prices-list/prices-list.component';
import { PriceCardComponent } from './prices-list/price-card/price-card.component';
import { ToggleButtonComponent } from './prices-list/toggle-button/toggle-button.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    PricesListComponent,
    PriceCardComponent,
    ToggleButtonComponent,
  ],
  imports: [CommonModule, StockPricesRoutingModule, SharedModule],
})
export class StockPricesModule {}
