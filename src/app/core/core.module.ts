import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PricesDataService } from './services/prices-data.service';
import { PricesDataServiceImp } from './services/prices-data.service.imp';
import { websocketService } from './services/websockets.service';
import { WebsocketServiceImp } from './services/websockets.service.imp';
import { HttpClientModule } from '@angular/common/http';
import { HttpPricesImpService } from './services/http-prices-imp.service';
import { HttpPricesService } from './services/http-prices.service';
@NgModule({
  declarations: [],
  imports: [CommonModule, HttpClientModule],
  providers: [
    { provide: PricesDataService, useClass: PricesDataServiceImp },
    { provide: websocketService, useClass: WebsocketServiceImp },
    { provide: HttpPricesService, useClass: HttpPricesImpService },
  ],
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule?: CoreModule) {
    if (parentModule) {
      throw new Error(
        'GreetingModule is already loaded. Import it in the AppModule only'
      );
    }
  }
}
