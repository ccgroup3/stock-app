import { AnonymousSubject } from 'rxjs/internal/Subject';

export abstract class websocketService {
  public abstract connect(url: string): AnonymousSubject<MessageEvent>;
}
