import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { load } from 'protobufjs';
import { environment } from 'environments/environment';
import { PricesDataService } from './prices-data.service';
import { websocketService } from './websockets.service';
import { WSMessage } from '../models/ws-msg.model';
@Injectable({
  providedIn: 'root',
})
export class PricesDataServiceImp implements PricesDataService {
  public prices: Subject<WSMessage>;
  message: WSMessage = {
    subscribe: ['AAPL', 'TSLA', 'MSFT', 'GOOG'],
  };
  constructor(private websocketService: websocketService) {
    this.prices = <Subject<WSMessage>>(
      this.websocketService.connect(environment.YAHOO_Finance_URL).pipe(
        map((response: MessageEvent): any => {
          return response.data;
        })
      )
    );

    setTimeout(() => {
      this.prices.next(this.message);
    }, 3000);
  }
  /**
   * Decode data with protobufJS package
   * @param data
   * @return decoded data
   */
  public async decodeData(data: string): Promise<any> {
    try {
      const root = await load('/assets/YPricingData.proto');
      const Yaticker = root.lookupType('yaticker');
      const ArrayBuffer = this.base64ToArrayBuffer(data);
      const TYPED_ARRAY = new Uint8Array(ArrayBuffer);
      const decoded = Yaticker.decode(TYPED_ARRAY);

      return decoded;
    } catch (error) {
      console.log({ error });
    }
  }
  /**
   * Convert from base64 to Array buffer
   * @param base64
   * @returns ArrayBuffer
   */
  private base64ToArrayBuffer(base64: string): ArrayBuffer {
    var binary_string = window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
      bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
  }
}
