import { Subject } from 'rxjs';

export abstract class PricesDataService {
  public prices: Subject<any>;
  public abstract decodeData(msg: string);
}
