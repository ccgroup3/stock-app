import { Observable } from 'rxjs';
import { Price } from '../models/price.model';

export abstract class HttpPricesService {
  public abstract getYahooFinancePrices(
    symbols: string
  ): Observable<Partial<Price>[]>;
}
