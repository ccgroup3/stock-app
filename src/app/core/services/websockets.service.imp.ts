import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';
import { AnonymousSubject } from 'rxjs/internal/Subject';
import { websocketService } from './websockets.service';

@Injectable({
  providedIn: 'root',
})
export class WebsocketServiceImp implements websocketService {
  private subject: AnonymousSubject<MessageEvent>;

  constructor() {}
  /**
   * Connect to websosket
   * @param url
   * @returns Subject
   */
  public connect(url: string): AnonymousSubject<MessageEvent> {
    if (!this.subject) {
      this.subject = this.create(url);
      console.log('Successfully connected: ' + url);
    }
    return this.subject;
  }
  /**
   * Create Websocket
   * @param url
   * @returns AnonymousSubject
   */
  private create(url: string): AnonymousSubject<MessageEvent> {
    let ws = new WebSocket(url);
    let observable = new Observable((obs: Observer<MessageEvent>) => {
      ws.onmessage = obs.next.bind(obs);
      ws.onerror = obs.error.bind(obs);
      ws.onclose = obs.complete.bind(obs);
      return ws.close.bind(ws);
    });

    let observer = {
      error: (err) => {
        console.log({ err });
      },
      complete: () => {},
      next: (data: Object) => {
        console.log({ WSReadyState: ws.readyState });

        if (ws.readyState === WebSocket.OPEN) {
          ws.send(JSON.stringify(data));
        }
      },
    };
    return new AnonymousSubject<MessageEvent>(observer, observable);
  }
}
