import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'environments/environment';
import { Price } from '../models/price.model';
import { HttpPricesService } from './http-prices.service';

@Injectable({
  providedIn: 'root',
})
export class HttpPricesImpService implements HttpPricesService {
  constructor(private http: HttpClient) {}
  public getYahooFinancePrices(symbols: string): Observable<Partial<Price>[]> {
    return this.http.get<Partial<Price>[]>(
      `${environment.Yahoo_finance_api}?symbols=${symbols}`
    );
  }
}
