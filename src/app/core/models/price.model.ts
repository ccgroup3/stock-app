export class HighLow {
  highest: number;
  lowest: number;
}
export class Price {
  id: string;
  price: number;
  weekly: HighLow;
  daily: HighLow;
}
